let playerOneHealth = document.getElementById('p1Health')
let playerTwoHealth = document.getElementById('p2Health')
let playerMove = document.getElementById('move')

class Player {

    constructor (name, health) {
        this.name = name
        this.health = health
    }

    powerAttack () {
        if (currentPlayer === 'Ninja') {
            Samurai.health -= 10
            playerTwoHealth.value -= 10
            if (Samurai.health === 0) {
                alert('SneakyBoi has won!!!')
                location.reload()
            }
            currentPlayer = 'Samurai'
            playerMove.innerHTML = 'Player 2 make your move...'
        } else if (currentPlayer === 'Samurai') {
            Ninja.health -= 10
            playerOneHealth.value -= 10
            if (Ninja.health === 0) {
                alert('LeeroyJenkins has won!!!')
                location.reload()
            }
            currentPlayer = 'Ninja'
            playerMove.innerHTML = 'Player 1 make your move'
        }
    }

    lightAttack () {
        if (currentPlayer === 'Ninja') {
            Samurai.health -= 5
            playerTwoHealth.value -= 5
            if (Samurai.health === 0) {
                alert('SneakyBoi has won!!!')
                location.reload()
            }
            currentPlayer = 'Samurai'
            playerMove.innerHTML = 'Player 2 make your move...'
        } else if (currentPlayer === 'Samurai') {
            Ninja.health -= 5
            playerOneHealth.value -= 5
            if (Ninja.health === 0) {
                alert('LeeroyJenkins has won!!!')
                location.reload()
            }
            currentPlayer = 'Ninja'
            playerMove.innerHTML = 'Player 1 make your move...'
        }
    }

    heal () {
        if (currentPlayer === 'Ninja') {
            Ninja.health += 10
            playerOneHealth.value += 10
            currentPlayer = 'Samurai'
            playerMove.innerHTML = 'Player 2 make your move...'
        } else if (currentPlayer === 'Samurai') {
            Samurai.health += 10
            playerTwoHealth.value += 10
            currentPlayer = 'Ninja'
            playerMove.innerHTML = 'Player 1 make your move...'
        }
    }
}

class superPlayer extends Player {

    constructor (name, health, armor) {
        super(name, health)
        this.armor = armor
    }

    superAttack () {
        if (currentPlayer === 'Dragon') {
            Ninja.health -= 20
            Samurai.health -= 20
            playerOneHealth.value -= 20
            playerTwoHealth.value -= 20
            currentPlayer = 'Ninja'
            playerMove.innerHTML = 'Player 1 make your move...'
        }
    }
}

//player 1
const Ninja = new Player('SneakyBoi', 100)
//player 2
const Samurai = new Player('LeeroyJenkins', 100)
//super player
const Dragon = new superPlayer('BigGuy', 50, 25)

let currentPlayer = 'Ninja'

let power = document.getElementById('powerAttack')

function powerMove() {
    if (currentPlayer === 'Ninja') {
        Samurai.powerAttack()
    } else if (currentPlayer === 'Samurai') {
        Ninja.powerAttack()
    }
}

power.addEventListener('click', powerMove)

let light = document.getElementById('lightAttack')

function lightMove() {
    if (currentPlayer === 'Ninja') {
        Samurai.lightAttack()
    } else if (currentPlayer === 'Samurai') {
        Ninja.lightAttack()
    }
}

light.addEventListener('click', lightMove)

let heal = document.getElementById('heal')

function healPlayer() {
    if (currentPlayer === 'Ninja') {
        Ninja.heal()
    } else if (currentPlayer === 'Samurai') {
        Samurai.heal()
    }
}

heal.addEventListener('click', healPlayer)